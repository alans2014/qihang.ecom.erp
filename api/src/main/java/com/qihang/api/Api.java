package com.qihang.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class Api {
    public static void main( String[] args )
    {
        SpringApplication.run(Api.class, args);
    }
}
